# Dribbble-Client
App client to get access to the most popular Shots on Dribbble social network through his API.

Librarys used in this app:

* [Picasso](http://square.github.io/picasso/)
* [Volley](https://developer.android.com/training/volley/index.html)
* [Crouton](https://github.com/keyboardsurfer/Crouton)
* [FloatActionButton](https://github.com/Clans/FloatingActionButton)